import React from "react";
import Navbar from "./components/Navbar";
import Search from "./components/Search/Search";
import "./App.css";

function App() {
  return (
    <div>
      <Navbar />
      <Search />
    </div>
  );
}

export default App;
