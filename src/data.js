import airFrance from "./assets/airFrance.png";
import klm from "./assets/klm.png";
import ryanair from "./assets/ryanair.png";
import brussels from "./assets/brussels.png";

const data = [
  {
    id: 1,
    price: 400,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "10.15 am",
    arrivalTime: "11.45 am",
    code: "FLL-6239",
    flightImg: airFrance,
    from: {
      city: "Amsterdam-AMS",
      short: "AMS"
    },
    to: {
      city: "Paris-PAR",
      short: "PAR"
    }
  },
  {
    id: 2,
    price: 850,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "12.20 pm",
    arrivalTime: "1.45 pm",
    code: "FLL-6239",
    flightImg: klm,
    from: {
      city: "Amsterdam-AMS",
      short: "AMS"
    },
    to: {
      city: "Paris-PAR",
      short: "PAR"
    }
  },
  {
    id: 3,
    price: 550,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "4.20 pm",
    arrivalTime: "6.35 pm",
    code: "FLL-6239",
    flightImg: airFrance,
    from: {
      city: "Paris-PAR",
      short: "PAR"
    },
    to: {
      city: "Amsterdam-AMS",
      short: "AMS"
    }
  },
  {
    id: 4,
    price: 380,
    depart: "2022-07-07",
    arrival: "2022-07-07",
    departTime: "7.05 pm",
    arrivalTime: "8.25 pm",
    code: "FLL-6239",
    flightImg: klm,
    from: {
      city: "Paris-PAR",
      short: "PAR"
    },
    to: {
      city: "Amsterdam-AMS",
      short: "AMS"
    }
  },
  {
    id: 5,
    price: 730,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "5.05 pm",
    arrivalTime: "6.25 pm",
    code: "FLL-6239",
    flightImg: airFrance,
    from: {
      city: "Paris-PAR",
      short: "PAR"
    },
    to: {
      city: "Franfurt-HHN",
      short: "HHN"
    }
  },
  {
    id: 6,
    price: 240,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "9.05 am",
    arrivalTime: "10.25 am",
    code: "FLL-6239",
    flightImg: ryanair,
    from: {
      city: "Paris-PAR",
      short: "PAR"
    },
    to: {
      city: "Franfurt-HHN",
      short: "HHN"
    }
  },
  {
    id: 7,
    price: 290,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "9.50 am",
    arrivalTime: "11.25 am",
    code: "FLL-6239",
    flightImg: airFrance,
    from: {
      city: "Franfurt-HHN",
      short: "HHN"
    },
    to: {
      city: "Paris-PAR",
      short: "PAR"
    }
  },
  {
    id: 8,
    price: 350,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "11.23 am",
    arrivalTime: "12.25 pm",
    code: "FLL-6239",
    flightImg: airFrance,
    from: {
      city: "Paris-PAR",
      short: "PAR"
    },
    to: {
      city: "Brussels-CRL",
      short: "CRL"
    }
  },
  {
    id: 9,
    price: 248,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "1.25 pm",
    arrivalTime: "2.45 pm",
    code: "FLL-6239",
    flightImg: brussels,
    from: {
      city: "Paris-PAR",
      short: "PAR"
    },
    to: {
      city: "Brussels-CRL",
      short: "CRL"
    }
  },
  {
    id: 10,
    price: 330,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "1.55 pm",
    arrivalTime: "3.10 pm",
    code: "FLL-6239",
    flightImg: airFrance,
    from: {
      city: "Brussels-CRL",
      short: "CRL"
    },
    to: {
      city: "Paris-PAR",
      short: "PAR"
    }
  },
  {
    id: 11,
    price: 340,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "4.05 pm",
    arrivalTime: "5.45 pm",
    code: "FLL-6239",
    flightImg: brussels,
    from: {
      city: "Brussels-CRL",
      short: "CRL"
    },
    to: {
      city: "Paris-PAR",
      short: "PAR"
    }
  },
  {
    id: 12,
    price: 510,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "1.23 pm",
    arrivalTime: "3.45 pm",
    code: "FLL-6239",
    flightImg: klm,
    from: {
      city: "Amsterdam-AMS",
      short: "AMS"
    },
    to: {
      city: "Franfurt-HHN",
      short: "HHN"
    }
  },
  {
    id: 13,
    price: 350,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "2.33 pm",
    arrivalTime: "3.27 pm",
    code: "FLL-6239",
    flightImg: ryanair,
    from: {
      city: "Amsterdam-AMS",
      short: "AMS"
    },
    to: {
      city: "Franfurt-HHN",
      short: "HHN"
    }
  },
  {
    id: 14,
    price: 450,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "4.19 pm",
    arrivalTime: "5.50 pm",
    code: "FLL-6239",
    flightImg: klm,
    from: {
      city: "Franfurt-HHN",
      short: "HHN"
    },
    to: {
      city: "Amsterdam-AMS",
      short: "AMS"
    }
  },
  {
    id: 15,
    price: 150,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "7.33 am",
    arrivalTime: "8.57 am",
    code: "FLL-6239",
    flightImg: ryanair,
    from: {
      city: "Franfurt-HHN",
      short: "HHN"
    },
    to: {
      city: "Amsterdam-AMS",
      short: "AMS"
    }
  },
  {
    id: 16,
    price: 654,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "9.14 am",
    arrivalTime: "10.45 am",
    code: "FLL-6239",
    flightImg: brussels,
    from: {
      city: "Amsterdam-AMS",
      short: "AMS"
    },
    to: {
      city: "Brussels-CRL",
      short: "CRL"
    }
  },
  {
    id: 17,
    price: 894,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "11.00 am",
    arrivalTime: "12.30 pm",
    code: "FLL-6239",
    flightImg: klm,
    from: {
      city: "Brussels-CRL",
      short: "CRL"
    },
    to: {
      city: "Amsterdam-AMS",
      short: "AMS"
    }
  },
  {
    id: 18,
    price: 457,
    depart: "2022-07-10",
    arrival: "2022-07-10",
    departTime: "12.45 pm",
    arrivalTime: "1.50 pm",
    code: "FLL-6239",
    flightImg: brussels,
    from: {
      city: "Brussels-CRL",
      short: "CRL"
    },
    to: {
      city: "Amsterdam-AMS",
      short: "AMS"
    }
  },
  {
    id: 19,
    price: 671,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "2.05 pm",
    arrivalTime: "3.10 pm",
    code: "FLL-6239",
    flightImg: brussels,
    from: {
      city: "Franfurt-HHN",
      short: "HHN"
    },
    to: {
      city: "Brussels-CRL",
      short: "CRL"
    }
  },
  {
    id: 20,
    price: 2500,
    depart: "2022-07-05",
    arrival: "2022-07-05",
    departTime: "3.15 pm",
    arrivalTime: "4.25 pm",
    code: "FLL-6239",
    flightImg: brussels,
    from: {
      city: "Franfurt-HHN",
      short: "HHN"
    },
    to: {
      city: "Brussels-CRL",
      short: "CRL"
    }
  }
];

export default data;
