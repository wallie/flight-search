import React from "react";

import Data from "./Data";

function Sample() {
  return (
    <div style={{ marginTop: "10px" }}>
      <div className="card">
        <div className="card-body">
          <div
            style={{
              color: "#138496",
              fontWeight: "bold",
              marginBottom: "10px"
            }}
          >
            Sample data
          </div>

          <Data
            origin={"Amsterdam"}
            dest={"Paris"}
            departdate={"2022-07-05"}
            returnDate={"2022-07-07"}
          />

          <Data
            origin={"Paris"}
            dest={"Franfurt"}
            departdate={"2022-07-05"}
            returnDate={"2022-07-05"}
          />
        </div>
      </div>
    </div>
  );
}

export default Sample;
