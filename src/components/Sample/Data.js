import React from "react";

function Data(props) {
  const { origin, dest, departdate, returnDate } = props;

  return (
    <div>
      <div className="row">
        <div className="col-md-6">Origin</div>
        <div className="col-md-6">{origin}</div>
      </div>
      <div className="row">
        <div className="col-md-6">Destination</div>
        <div className="col-md-6">{dest}</div>
      </div>
      <div className="row">
        <div className="col-md-6">Departure</div>
        <div className="col-md-6">{departdate}</div>
      </div>
      <div className="row" style={{ marginBottom: "10px" }}>
        <div className="col-md-6">Return</div>
        <div className="col-md-6">{returnDate}</div>
      </div>
    </div>
  );
}

export default Data;
