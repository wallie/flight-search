import React from "react";

function Flight(props) {
  const { passengerCount, setFlight } = props;
  let { filteredData } = props;

  const handleFlightSelect = (flight) => {
    setFlight(flight);
  };

  return (
    <>
      {filteredData.map((data, i) => {
        return (
          <div
            className="card mb-3"
            id="ticket_card"
            key={i}
            style={{
              borderColor:
                data.selected !== undefined && data.selected ? "#138496" : null,
            }}
          >
            <div className="card-body" onClick={() => handleFlightSelect(data)}>
              <div style={{ display: "flex" }}>
                <div
                  style={{
                    width: "60%",
                    fontSize: "13px",
                    display: "flex",
                    lineHeight: "1.5rem",
                  }}
                >
                  <div>
                    <div
                      style={{
                        marginBottom: "6px",
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <span>
                        <b>€ {data.price * passengerCount}</b>
                      </span>
                    </div>
                    <div>
                      <b>
                        {data.from.short} {">>"} {data.to.short}
                      </b>
                    </div>
                    <div>{data.code}</div>
                    <div>Depart: {data.departTime}</div>
                    <div>Arrive: {data.arrivalTime}</div>
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    width: "40%",
                    flexDirection: "column",
                  }}
                >
                  <div style={{ height: "100px", width: "140px" }}>
                    <img
                      src={data.flightImg}
                      alt="flight_img"
                      style={{ height: "100%", width: "100%" }}
                    />
                  </div>
                  <div>
                    <button
                      type="button"
                      className="btn btn-sm btn-info"
                      onClick={() => handleFlightSelect(data)}
                    >
                      <b>Select</b>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
}

export default Flight;
