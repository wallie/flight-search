/* eslint-disable array-callback-return */
import React, { useState } from "react";
import Result from "./Result";
import Sample from "../Sample/Sample";
import Overview from "../Overview/Overview";
import flightData from "../../data";

function Search() {
  const [passengerCount, setPassengerCount] = useState(1);

  const [originCity, setOriginCity] = useState("");
  const [destinationCity, setDestinationCity] = useState("");
  const [departureDate, setDepartureDate] = useState("");
  const [returnDate, setReturnDate] = useState("");
  const [departureFilterData, setDepartureFilterData] = useState([]);
  const [returnFilterData, setReturnFilterData] = useState([]);

  const [departureFlight, setDepartureFlight] = useState({});
  const [returnFlight, setReturnFlight] = useState({});

  const [isSearchClicked, setIsSearchClicked] = useState(false);
  const [isContinueClicked, setIsContinueClicked] = useState(false);

  const [alertMessage, setAlertMessage] = useState(null);

  const handleCount = (key) => {
    if (key === "add") {
      if (passengerCount >= 5) {
        return;
      }
      setPassengerCount(passengerCount + 1);
    } else if (key === "less") {
      if (passengerCount === 1) {
        return;
      }
      setPassengerCount(passengerCount - 1);
    }
  };

  const handleFocus = (e) => {
    e.currentTarget.type = "date";
  };
  const handleBlur = (e) => {
    e.currentTarget.type = "text";
  };

  const departureFilter = () => {
    let result = flightData.filter((data) => {
      if (
        data &&
        data?.from?.city &&
        data.from.city
          .toLowerCase()
          .includes(originCity.trim().toLowerCase()) &&
        data?.to?.city &&
        data.to.city
          .toLowerCase()
          .includes(destinationCity.trim().toLowerCase()) &&
        data.depart === departureDate
      ) {
        return data;
      }
    });
    setDepartureFilterData(result);
  };
  const returnFilter = () => {
    let result = flightData.filter((data) => {
      if (
        data &&
        data?.from?.city &&
        data.from.city
          .toLowerCase()
          .includes(destinationCity.trim().toLowerCase()) &&
        data?.to?.city &&
        data.to.city.toLowerCase().includes(originCity.trim().toLowerCase()) &&
        data.depart === returnDate
      ) {
        return data;
      }
    });
    setReturnFilterData(result);
  };

  const handleSearch = () => {
    setAlertMessage(null);

    if (!originCity) {
      setAlertMessage("Origin city can't be empty!");
    } else if (!destinationCity) {
      setAlertMessage("Destination city can't be empty!");
    } else if (!departureDate) {
      setAlertMessage("Departure date can't be empty!");
    } else if (!returnDate) {
      setAlertMessage("Return date can't be empty!");
    }
    if (originCity && destinationCity && departureDate && returnDate) {
      setIsSearchClicked(true);

      departureFilter();
      returnFilter();
    }
  };

  const updateSelected = (data, flight) => {
    const newData = data.map((d) =>
      d.id === flight.id ? { ...d, selected: true } : { ...d, selected: false }
    );

    return newData;
  };

  const setDeparture = (departureFlight) => {
    setDepartureFlight(departureFlight);

    const updatedDepartureData = updateSelected(
      departureFilterData,
      departureFlight
    );
    setDepartureFilterData(updatedDepartureData);
  };

  const setReturn = (returnFlight) => {
    setReturnFlight(returnFlight);

    const updatedReturnData = updateSelected(returnFilterData, returnFlight);
    setReturnFilterData(updatedReturnData);
  };

  const continueToBook = () => {
    setAlertMessage(null);

    if (
      Object.keys(departureFlight).length !== 0 &&
      Object.keys(returnFlight).length !== 0
    ) {
      setIsContinueClicked(true);
    } else {
      setAlertMessage("Please select a departure and return flight");
    }
  };

  const backToBook = () => {
    setIsContinueClicked(false);
  };

  return (
    <>
      {isContinueClicked ? (
        <div>
          <Overview
            departureFlight={departureFlight}
            returnFlight={returnFlight}
            passengerCount={passengerCount}
            backToBook={backToBook}
          />
        </div>
      ) : (
        <div>
          {alertMessage ? (
            <div style={{ marginTop: "10px" }}>
              <div
                className="alert alert-danger"
                role="alert"
                style={{ textAlign: "center", fontWeight: "bold" }}
              >
                {alertMessage}
              </div>
            </div>
          ) : null}

          <div className="row mt-4 ml-5 mr-5 mb-4">
            <div className="col-md-4">
              <div className="card">
                <div className="card-body">
                  <input
                    type="text"
                    placeholder="Origin city"
                    className="form-control mt-2"
                    onChange={(e) => {
                      setOriginCity(e.target.value);
                      setAlertMessage(null);
                    }}
                  />
                  <input
                    type="text"
                    placeholder="Destination city"
                    className="form-control mt-2"
                    onChange={(e) => {
                      setDestinationCity(e.target.value);
                      setAlertMessage(null);
                    }}
                  />
                  <input
                    type="text"
                    placeholder="Departure date"
                    className="form-control mt-2"
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                    onChange={(e) => {
                      setDepartureDate(e.target.value);
                      setAlertMessage(null);
                    }}
                  />
                  <input
                    type="text"
                    placeholder="Return date"
                    className="form-control mt-2"
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                    onChange={(e) => {
                      setReturnDate(e.target.value);
                      setAlertMessage(null);
                    }}
                  />
                  <div
                    className="d-flex justify-content-center mt-2"
                    style={{ alignItems: "center" }}
                  >
                    <button
                      type="button"
                      className="btn btn-secondary mr-2"
                      onClick={() => handleCount("less")}
                    >
                      -
                    </button>
                    <div className="text-muted">
                      {passengerCount} passengers
                    </div>
                    <button
                      type="button"
                      className="btn btn-secondary ml-2"
                      onClick={() => handleCount("add")}
                    >
                      +
                    </button>
                  </div>
                  <div>
                    <button
                      type="button"
                      className="btn btn-info search_btn"
                      onClick={handleSearch}
                    >
                      <b>Search</b>
                    </button>
                  </div>
                </div>
              </div>
              <Sample />
            </div>
            <div className="col-md-8">
              <Result
                departureFilterData={departureFilterData}
                returnFilterData={returnFilterData}
                isSearchClicked={isSearchClicked}
                passengerCount={passengerCount}
                setDeparture={setDeparture}
                setReturn={setReturn}
                continueToBook={continueToBook}
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default Search;
