import React from "react";
import forwardArrow from "../../assets/Forward.svg";
import Flight from "./Flight";
import NoResults from "./NoResults";

function Result(props) {
  const {
    departureFilterData,
    isSearchClicked,
    returnFilterData,
    passengerCount,
    setDeparture,
    setReturn,
    continueToBook
  } = props;

  const setResultDeparture = (flight) => {
    setDeparture(flight);
  };

  const setResultReturn = (flight) => {
    setReturn(flight);
  };

  return (
    <div className="card" style={{ height: "100%" }}>
      <div className="card-body">
        {!isSearchClicked &&
        departureFilterData.length === 0 &&
        returnFilterData.length === 0 ? (
          <NoResults noDataReturned={false} />
        ) : isSearchClicked &&
          (departureFilterData.length === 0 ||
            returnFilterData.length === 0) ? (
          <NoResults noDataReturned={true} />
        ) : (
          <div>
            <div className="mb-4">
              <div
                style={{
                  fontSize: "20px",
                  fontWeight: "bold"
                }}
              >
                Available Flights
              </div>
            </div>
            <div>
              <div className="row">
                <div className="col">
                  <div style={{ color: "#138496", fontWeight: "bold" }}>
                    Departure Flight
                    <p>{departureFilterData[0].depart}</p>
                  </div>
                  <Flight
                    filteredData={departureFilterData}
                    passengerCount={passengerCount}
                    setFlight={setResultDeparture}
                  />
                </div>
                <div className="col">
                  <div style={{ color: "#138496", fontWeight: "bold" }}>
                    Return Flight
                    <p>{returnFilterData[0].depart}</p>
                  </div>
                  <Flight
                    filteredData={returnFilterData}
                    passengerCount={passengerCount}
                    setFlight={setResultReturn}
                  />
                </div>
              </div>
            </div>
            <div>
              <div className="row">
                <div className="col">
                  <button
                    type="button"
                    className="btn btn-info search_btn"
                    onClick={continueToBook}
                  >
                    <b>Continue</b>
                    <span>
                      <img src={forwardArrow} alt="arrow" />
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Result;
