import React, { useState, useEffect } from "react";
import forwardArrow from "../../assets/Forward.svg";
import FlightSummary from "./FlightSummary";

function Overview(props) {
  const { departureFlight, returnFlight, passengerCount, backToBook } = props;

  const [passengers, setPassengers] = useState([]);

  const [booked, setBooked] = useState(false);
  const [inComplete, setIncomplete] = useState(false);

  useEffect(() => {
    const temp = [];
    for (var i = 0; i < passengerCount; i++) {
      temp.push({
        name: "",
        surname: ""
      });
    }
    setPassengers(temp);
  }, [passengerCount]);

  const updatePassengerName = (data, index) => {
    const newArr = passengers.map((item, i) => {
      if (index === i) {
        return { ...item, name: data };
      } else {
        return item;
      }
    });
    setPassengers(newArr);
  };

  const updatePassengerSurname = (data, index) => {
    const newArr = passengers.map((item, i) => {
      if (index === i) {
        return { ...item, surname: data };
      } else {
        return item;
      }
    });
    setPassengers(newArr);
  };

  const makeBooking = () => {
    setIncomplete(false);

    const infoCompleted = passengers.every((p) => {
      if (p?.name === "" || p?.surname === "") {
        return false;
      }
      return true;
    });

    if (infoCompleted) {
      setBooked(true);
    } else {
      setIncomplete(true);
    }
  };

  return (
    <div>
      {booked ? (
        <div style={{ marginTop: "10px" }}>
          <div
            className="alert alert-info"
            role="alert"
            style={{ textAlign: "center", fontWeight: "bold" }}
          >
            Booking made. Enjoy your flight with us.
          </div>
        </div>
      ) : null}

      {inComplete ? (
        <div style={{ marginTop: "10px" }}>
          <div
            className="alert alert-danger"
            role="alert"
            style={{ textAlign: "center", fontWeight: "bold" }}
          >
            Please make sure all passenger details have been completed.
          </div>
        </div>
      ) : null}

      <div className="row mt-4 ml-5 mr-5">
        <div className="col-md-5">
          <div className="card">
            <div className="card-body">
              <div
                style={{
                  color: "#138496",
                  fontWeight: "bold",
                  paddingBottom: "10px"
                }}
              >
                Flight Summary
              </div>

              <FlightSummary
                flight={departureFlight}
                type={"Departure"}
                passengerCount={passengerCount}
              />

              <FlightSummary
                flight={returnFlight}
                type={"Return"}
                passengerCount={passengerCount}
              />
            </div>
          </div>
        </div>
        <div className="col-md-7">
          <div className="card">
            <div className="card-body">
              <div
                style={{
                  color: "#138496",
                  fontWeight: "bold",
                  paddingBottom: "10px"
                }}
              >
                Passengers
              </div>
              {passengers.map((passenger, i) => {
                return (
                  <div
                    className="card"
                    key={i}
                    style={{
                      marginBottom: "10px",
                      display: "flex",
                      justifyContent: "space-between"
                    }}
                  >
                    <div className="card-body">
                      <div style={{ fontWeight: "bold" }}>
                        Passenger {i + 1}
                      </div>
                      <div className="row">
                        <div className="col">
                          <input
                            type="text"
                            placeholder="First name"
                            className="form-control mt-2"
                            onChange={(e) => {
                              updatePassengerName(e.target.value, i);
                              setIncomplete(false);
                            }}
                          />
                        </div>
                        <div className="col">
                          <input
                            type="text"
                            placeholder="Last name"
                            className="form-control mt-2"
                            onChange={(e) => {
                              updatePassengerSurname(e.target.value, i);
                              setIncomplete(false);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div>
            <div className="row">
              <div className="col-md-4">
                <button
                  type="button"
                  className="btn btn-light search_btn"
                  onClick={() => backToBook()}
                >
                  <b>Back</b>
                </button>
              </div>
              <div className="col-md-8">
                <button
                  type="button"
                  className="btn btn-info search_btn"
                  onClick={makeBooking}
                >
                  <b>Book</b>
                  <span>
                    <img
                      src={forwardArrow}
                      alt="arrow"
                      className="ml-2"
                      style={{ paddingBottom: "2px" }}
                    />
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Overview;
