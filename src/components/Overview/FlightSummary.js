import React from "react";

function FlightSummary(props) {
  const { flight, type, passengerCount } = props;

  return (
    <div
      className="card"
      style={{
        marginBottom: "10px",
        display: "flex",
        justifyContent: "space-between"
      }}
    >
      <div className="card-body">
        <div>
          <div
            style={{
              marginBottom: "6px",
              display: "flex",
              justifyContent: "space-between"
            }}
          >
            <span>
              <b>{type} Flight</b>
            </span>
            <span>
              <b>€ {flight.price * passengerCount}</b>
            </span>
          </div>
          <div>
            <b>
              {flight.from.short} {">>"} {flight.to.short}
            </b>
          </div>
          <div>{flight.code}</div>
          <div>
            Depart: {flight.depart} {flight.departTime}
          </div>
          <div>
            Arrive: {flight.depart} {flight.arrivalTime}
          </div>
        </div>
      </div>
    </div>
  );
}

export default FlightSummary;
