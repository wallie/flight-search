import React from 'react';

function Navbar() {
  return (
    <div>
      <nav className="navbar navbar-light navbar_container">
        <div className="navbar-brand mr-2 ml-5 navbar_title">
          Flight Search Project
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
