# Flight Search Project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Hosting

The site is hosted on Gitlab Pages and uses a CICD pipeline to build and deploy.\
Open [https://wallie.gitlab.io/flight-search](https://wallie.gitlab.io/flight-search) to view the live site in your browser.

## Sample Data

Amsterdam >> Paris (2022-07-05 -> 2022-07-07)\
Paris >> Franfurt (2022-07-05 -> 2022-05-05)

## Future Enhancements

Implement a state management library like Redux to help keep track of overall state.\
This will help with managing state and remove the need to pass props back and forth to mutiple parent child relations.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
